#!/bin/bash
#Todo: make password input parameter
# requires file ips.txt with ips of tools servers on newlines
# export SSHPASS='your deployer password'
if [ -z "$SSHPASS" ]
then
	echo "Please set env var SSHPASS"
	exit 1
fi

cat ips.txt | while read line 
do
   echo Starting ORCA on $line
#   sshpass -p 'ctrain!Lab' deployer@$line 'cd /cenx/ORCA/training;/cenx/xfer/start_cenx.sh'
   sshpass -e  ssh -o StrictHostKeyChecking=no deployer@$line 'cd /cenx/ORCA/training;source ~/.bash_profile;orca status'
done