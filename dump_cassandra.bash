#!/bin/bash



if [ "$#" -ne 1 ]; then
    echo "$0: Defaults to dumping localhost. Pass hostname argument if you wish to dump somewhere else..."
    HOST=localhost
else    
    HOST="$1"
fi

cqlsh $HOST -e "copy orange_internal_system_cenx.entity to 'mw_entity.csv' with header = true;"
#cqlsh $HOST -e "copy devcenx.link to 'mw_link.csv' with header = true;"

cat mw_entity.csv | tail -n +2 | cut -d, -f4- | jq -r . | jq . > mw_entity2.csv
mv mw_entity2.csv mw_entity.csv

