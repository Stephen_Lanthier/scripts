import os
import re
import csv
import string
import random
from shutil import copyfile
from glob import glob
from faker import Faker
from faker.providers import internet
import codecs

debug=1 
#sudo -H pip install Faker
 
# HEY YOU!! 
# This is list of fields we are looking for to anonymize....
# Go through each field of your IGOR CSV files and find what contains sensitive data...
# You will want to edit for each project based on what Igor generates

listOfFields = ["holder", "displayName", "siteId", "siteName", "postalCode", "city", "address", "equipmentId", "equipmentName", "ipAddress"]

# ALSO !
# IF you have specific needs for type of data -> e.g. address should be a random address instead of random string, 
# see the if statement that starts with CUSTOMIZE ME!

#What about model, circuitName, vendor, equipmentType, trailName, region
#fieldNames is fields (csv headers) specific for each file
fieldNames = []
fake = Faker()


def generate_address():
    return fake.street_address()

def generate_fullname():
    name = fake.first_name() + " " + fake.last_name()
    return name

def generate_phone():
    return fake.phone_number()

def generate_msisdn():
    return fake.msisdn()

def generate_city():
    return fake.city()

def generate_country():
    return fake.country()

def generate_zipcode():
    return fake.zipcode_in_state()

def generate_ipv4():
    return fake.ipv4_private()

def generate_latitude():
    return fake.latitude()

def generate_longitude():
    return fake.longitude()

def generate_company():
    return fake.company()

def generate_clli():
    clli=''.join(random.choice(string.ascii_uppercase + string.digits) for _ in range(8))
    return clli

def generate_email():
    email=fake.ascii_email(*args, **kwargs)
    return email 

def generate_date():
    return fake.date(pattern='%Y-%m-%d', end_datetime=None)

def anonymize_file():
    # pass file as input parameter. requires filename and field to anonymize
    return

def get_field_number():
    #
    return

def get_fields(file):
    # returns a list of fields from file as CSV
    with open(file,'r') as csvfile:
        reader = csv.DictReader(csvfile)
        for field in reader.fieldnames:
            if debug : print("DEBUG [get_fields]: Found field " + field)
    csvfile.close()
    return reader.fieldnames

# Return all string values from file "file" with CSV fieldName "fieldName"
def get_all_values(file,fieldName):
    allValues = []
    if (os.stat(file).st_size == 0):
         if debug : print("DEBUG[get_all_values] file " + file + " is empty, skipping")
    else:
        with open(file,'r') as csvfile:
                reader = csv.DictReader(csvfile)
                # is fieldName in reader.fieldnames ? if not, skip file
                #print("DEBUG fieldName is " + fieldName + " File is " + file )
                if fieldName in reader.fieldnames:
                    for row in reader:
                        #print("DEBUG [get_all_values]: Value of " + fieldName + " in file " + file + ":" + row[fieldName])
                        allValues.append(row[fieldName])
    return allValues


def get_files_in_cwd():
    # return each file in cwd
    #csvFiles = []
    #dirlist = os.listdir(".")
    #pattern = re.compile(r'\.csv')
    #for file in dirlist:
    #    if pattern.search(file):
    #        print("DEBUG: " + file)
    #        csvFiles.append(file)
    #    else:
    #        print("DEBUG ***Non-csv file found " + file)
    csvFiles = [y for x in os.walk(".") for y in glob(os.path.join(x[0], '*.csv'))]
    for file in csvFiles:
         if debug : print("DEBUG[get_files_in_cwd]: file is " + file)
    return csvFiles

def backup_file(file):
    # backup each file in folder
    pattern = re.compile(r'csv', re.IGNORECASE)
    newFile = pattern.sub('bak', file)
    if os.path.exists(newFile):
        if debug : print("DEBUG [backup_file]: Destination file exists! Not overwriting! " + newFile)
    else:
        if debug : print("DEBUG [backup_file]: Copy old file " + file + " newfile: ." + newFile)
        #copyfile(file, "." + newFile)
        copyfile(file, newFile)
    return

#This function removes non-ascii characters from strings
def removeNonAscii(s): 
    return "".join(i for i in s if ord(i)<128)

#iteratively go through csv files and remove non-ascii characters that will trip us up later
def fix_ascii(file):
    newfile=file + ".fixed"
    s = open(file,"r+")
    w = codecs.open(newfile,"w+", encoding='utf8')
    for line in s.readlines():
        line=removeNonAscii(line)
        w.write(line)
    s.close()
    w.close()
    copyfile(newfile,file)

def fix_file(file,string,newstring):
    #print("DEBUG [fix_file] : input file is " + file + " input string is " + string + " change to : " + newstring)   
    newfile=file + ".new"
    s = codecs.open(file,"r", encoding='utf8')
    w = codecs.open(newfile,"w+", encoding='utf8')
    for line in s.readlines():
        #removeNonAscii(line)
        newline=line.replace(string,newstring)
        if debug : 
            if (newline != line): print("DEBUG [fix_file]: File " + file + " fixed line is  "+newline)   
        w.write(newline)
    s.close()
    w.close()
    copyfile(newfile,file)

def existsInFile(file,string):
    with open(file,"r") as myfile:
     if string in myfile.read():
         if debug : print('DEBUG[existsInFile] string ' + string + " found in file " + file)
         return 1
    return 0
####MAIN###################################################

# Store list of CSV files in a list
csvFilesList = get_files_in_cwd()
for file in csvFilesList:
    backup_file(file)
    fix_ascii(file)

output=open('commands.sh','w+')

#updateDict = dict() #dictionary of old>new values
# Create your dictionary class 
class myDict(dict): 
  
    # __init__ function 
    def __init__(self): 
        self = dict() 
          
    # Function to add key:value 
    def add(self, key, value): 
        if key in self.keys():
            print("key " + key + " already exists")
        else:    
            self[key] = value 

updateDict = myDict() #dictionary of old>new values
   
#This main loop builds a list of oldvalues > newvalues and stores in a dictionary (updateDict)
for file in csvFilesList:
    if debug : print("DEBUG [main]: ************************ Processing/analyzing file " + file)
    if (os.stat(file).st_size == 0):
        if debug : print("DEBUG [main]: File " + file + " is empty. Skipping analysis.")
        continue
    fieldNames = get_fields(file)
    for field in fieldNames:   
        # Only look for field if it's in our target list
        if field in listOfFields:
            #print("DEBUG [main]: Found target field : " + field + " in file " + file)
            #currentValues is list of all possible values for current field
            
            #Only replace the value if it ISN'T pre-pended with an undercore
            currentValues = []
            currentValues.extend(get_all_values(file,field))
            #Now iterate through the files (nested) again to find if this field exists 
            #print("DEBUG [main]: now let's iterate through the other files looking for field " + field)
            #for file2 in csvFilesList:
                #currentValues.extend(get_all_values(file2,field))
            myuniqueset = list(set(currentValues))
            #print(currentValues)
            #print("DEBUG [main]: Unique list from current source file:" + file2 + " master reference file:" + file + " field " + field + " set (nextline): "  )
            #print(myuniqueset)    
#            raw_input("Press Enter to continue...")  
            
            #item is all current values from current source file of field searched for

            for item in myuniqueset:
                #newitem=''.join(random.choice(string.ascii_uppercase + string.digits) for _ in range(8))
                # CUSTOMIZE ME!! Based on what your csv header fields are and what data you'd like to generate
                if (field == "address"):
                    newitem=generate_address()
                elif (field == "ipAddress"):
                    newitem=generate_ipv4()
                elif (field == "company"):
                    newitem=generate_company()
                elif (field == "postalCode"):
                    newitem=generate_zipcode()
                elif (field == "city"):
                    newitem=generate_city()
                elif (field == ""):
                    if debug : print "DEBUG[main]: blank field found"
                elif (field == " "):
                    if debug : print "DEBUG[main]: blank \" \" field found"
                else:
                    newitem=generate_clli()                    
               #We need to iterate one more time!
                #This crazy 3 lines to remove anything after a slash, so we don't mess up sed command
                pattern=re.compile(r'/') 
                templist=pattern.split(item,0)
                item=templist[0]
                #Better if we replace dot "." with "\."
                pattern=re.compile(r'\.')
                item=pattern.sub('\.',item,3)
                #print("DEBUG [main] about to fix all files replacing fieldtype " + field + " item:" + item + " with:" + newitem)
                # need to make sure item isn't blank or whitespace or just . or ? or *
                #if ((item == "." ) or ( item == "?" ) or ( item == "*" ) or ( item == ".*")):
                    #print("DEBUG [main]: wildcard found" + item )
                    #raw_input("Press Enter to continue...")
                #elif (item and item.strip()):
                    #print "DEBUG [main]: not blank string"
                    #for file3 in csvFilesList:
                        #print("sed -i \'s/" + item + "/" + newitem +"/g\' " + "$file1" + " > " + "$file2" + "\n" )
#                print("DEBUG: File: " + file + " field: " + field + " value " + item + " new value " + newitem )
#                print("sed -i \'s/" + item + "/" + newitem +"/g\' " + " $file1 " + " > " + " $file2 " + "\n" )
                output.write("sed -i \'s/" + item + "/" + newitem +"/g\' " + " $file1 " + " > " + " $file2 " + "\n" )
                #TODO don't add duplicate if item already exists
                if ((item == "." ) or ( item == "?" ) or ( item == "*" ) or ( item == ".*")):
                    if debug : print("DEBUG [main]: wildcard found" + item )
                elif (len(item) < 3):
                    if debug : print("DEBUG [main] string " + item + " too short to change")
                elif (item and item.strip()):
                    updateDict.add(item, newitem)
                else:
                    if debug : print("DEBUG [main] blank key found in file " + file + " for field " + field)
                #TODO push items to a list of things we already found - and make sure we don't do them twice!

                #raw_input("Press Enter to continue...")
                        #output.write("mv " + file3 + ".new" + " " + file3 + "\n" )
                        
                        #Temporary disable fixing the files:
                        #fix_file(file3,output,item,newitem)
                #else:
                    #print "DEBUG [main] : blank string found!!! why????"
                    #TODO: Still getting blank string in list of items to replace. Figure out where it's coming from
 #                   raw_input("Press Enter to continue...")


 #DEBUG ONLY iterate through values
if debug: 
    for key in updateDict:
        print("DEBUG[main 2nd loop] updateDict old value is: " + key + " new value is " + updateDict[key])

#Here's where we check our files for the keys we generated - if it exists, replace in spot.
raw_input("About to edit files. Press Enter to continue...")
for key in updateDict:
    for file in csvFilesList:
        if existsInFile(file,key):
            if debug: print("DEBUG[main 3rd loop] found " + key + " in file " + file + " changing to " + updateDict[key])
            fix_file(file,key,updateDict[key])

print("All done!")
