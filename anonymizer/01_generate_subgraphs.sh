
SERVER='ps02.cenx.localnet:8080'


echo TOKEN = 'curl -X POST "http://'$SERVER'/cenx/api-docs/session/login" -H  "accept: application/json" -H  "content-type: application/x-www-form-urlencoded" -d "username=ikent&password=tester&domain=ldap"'
TOKEN=`curl -X POST "http://ps02.cenx.localnet:8080/cenx/api-docs/session/login" -H  "accept: application/json" -H  "content-type: application/x-www-form-urlencoded" -d "username=ikent&password=tester&domain=ldap" | cut -d\" -f4`
echo TOKEN is $TOKEN
read
#hard code token if needed:
#TOKEN='7641b84859a82fcd9d1143bebf8d1dbe'


execute_curl () {
	TYPE=$1
	ID=$2
	count=$3
	echo "Executing curl command to grab subgraph for $TYPE with id $ID and putting in folder $3"
	curl -X GET "http://$SERVER/cenx/api-docs/topology/subgraph/$TYPE/$ID?token=$TOKEN&fields=*" -H "accept: application/json"  > subgraph$count.json
	java -jar igor-json-2-csv-0.0.1-SNAPSHOT-jar-with-dependencies.jar subgraph$count.json; 
	mkdir $count; 
	cp sim_types.csv sim_types.bak
	#Strip out unwanted services from PDH_Circuit and IP_Consumer (usually discovered by Cellsite and Hubsite walks)
	sed -e 's/,SIMContainer,,,,,/,SIMContainer,,displayName,,,/g' -e 's/,SIMSNC,,,,,/,SIMSNC,,displayName,TRUE,,/g' -e 's/,SIMLTP,,,,,/,SIMLTP,,displayName,,,/' -e 's/,SIMLinkCon,,,,,/,SIMLinkCon,,displayName,,,/' sim_types.bak > sim_types.csv
	egrep -e 'displayName|TR016Z4T4|TR003D7NK|TR002A16S|TR00317LA' PDH_Circuit.csv > PDH_Circuit.new
	mv PDH_Circuit.new PDH_Circuit.csv
	egrep -e 'displayName|TR00001PD|TR00002TR|TR000055I|TR00000IU|TR00000P8' IP_Consumer.csv > IP_Consumer.new
	mv IP_Consumer.new IP_Consumer.csv
	mv *.csv $count;
	folder_path+=" $count""/"
}


#TYPE='PDH_Circuit'
#ID='TR016Z4T4'
count=1
execute_curl "PDH_Circuit" "TR016Z4T4" $count ; count=$((count+1))
execute_curl "CellSite" "0411M" $count; count=$((count+1))
execute_curl "CellSite" "6512M" $count; count=$((count+1)) 
execute_curl "HubSite" "8359M" $count; count=$((count+1))

execute_curl PDH_Circuit TR003D7NK  $count ; count=$((count+1))
execute_curl HubSite 0850W $count ; count=$((count+1))
execute_curl CellSite 1802W $count ; count=$((count+1))
execute_curl CellSite 9502W $count ; count=$((count+1))
execute_curl HubSite F171W $count ; count=$((count+1))


execute_curl PDH_Circuit TR002A16S    $count ; count=$((count+1))
execute_curl CellSite 0782B  $count ; count=$((count+1))
execute_curl CellSite 0787B  $count ; count=$((count+1))
execute_curl CellSite 0962B  $count ; count=$((count+1))
execute_curl CellSite 0963B   $count ; count=$((count+1))
execute_curl HubSite 5297B  $count ; count=$((count+1))


 execute_curl PDH_Circuit TR00317LA $count ; count=$((count+1))
 execute_curl CellSite 0805S $count ; count=$((count+1))
 execute_curl CellSite 5119S $count ; count=$((count+1))


 execute_curl IP_Consumer TR00001PD $count ; count=$((count+1))
execute_curl CellSite 5186D $count ; count=$((count+1))
execute_curl CellSite 9530D $count ; count=$((count+1))

execute_curl IP_Consumer TR00002TR  $count ; count=$((count+1))
execute_curl HubSite 0829S  $count ; count=$((count+1))
execute_curl HubSite 2940S  $count ; count=$((count+1))

execute_curl IP_Consumer TR000055I   $count ; count=$((count+1)) #####fix id
execute_curl CellSite 0081B  $count ; count=$((count+1))

execute_curl IP_Consumer TR00000IU $count ; count=$((count+1))
execute_curl CellSite 7884M $count ; count=$((count+1))


execute_curl IP_Consumer TR00000P8  $count ; count=$((count+1))
execute_curl  CellSite 6994M $count ; count=$((count+1))
execute_curl  CellSite 7350M $count ; count=$((count+1))

echo "Clean up subgraphs we generated.... putting into folder subgraphs"
mkdir subgraphs
mv subgraph*.json subgraphs/

echo "Merging sim_types.csv."
#merge sim_types to one, remove the others:
cat */sim_types.csv > sim_types_merged.csv
#find the header rows first
grep searchable sim_types_merged.csv|sort -u  > sim_types.csv
#find non header rows (should not contain "searchable" keyword)
grep -v searchable sim_types_merged.csv |sort -u >> sim_types.csv 

rm */sim_types.csv
#put sim_types into 1st folder
cp sim_types.csv 1/sim_types.csv

echo "Master sim_types.csv file is in folder 1/. Please view the file make sure no duplicate headers or other issues."
echo
echo

echo NEXT COMMAND IS:

echo "java -jar  igor-5.0.0-SNAPSHOT-jar-with-dependencies.jar --out-directives directives/generated/ --out-red generated/ --in-visualization directives/specifications/ui.js --out-other other/ $folder_path" > 03_igor_upload_command.sh 
echo
echo "python 02_anonymize.py # optional"
echo "bash 03_igor_upload_command.sh"
echo "Remember to update sim files, edit ui.js to remove unnecessary entities, and copy over security.js"




