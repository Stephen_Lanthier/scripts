
SERVER='ps02.cenx.localnet:8080'
TOKEN='948e8a8c9dd9db06b4faed3db748746a'


echo TOKEN = curl -X POST "http://$SERVER/cenx/api-docs/session/login" -H  "accept: application/json" -H  "content-type: application/x-www-form-urlencoded" -d "username=ikent&password=tester&domain=ldap"
read

execute_curl () {
	TYPE=$1
	ID=$2
	count=$3
	curl -X GET "http://$SERVER/cenx/api-docs/topology/subgraph/$TYPE/$ID?token=$TOKEN&fields=*" -H "accept: application/json"  > subgraph$count.json
	java -jar igor-json-2-csv-0.0.1-SNAPSHOT-jar-with-dependencies.jar subgraph$count.json; 
	mkdir $count; 
	cp sim_types.csv sim_types.bak
	sed -e 's/,SIMContainer,,,,,/,SIMContainer,displayName,TRUE,,,/g' -e 's/,SIMSNC,,,,,/,SIMSNC,,displayName,TRUE,,/g' -e 's/,SIMLTP,,displayName,,,/,SIMLTP,,displayName,,,/' -e 's/,SIMLinkCon,,displayName,,,/,SIMLinkCon,,displayName,,,/' sim_types.bak > sim_types.csv
	mv *.csv $count;
	folder_path+=" $count""/"
}


#TYPE='PDH_Circuit'
#ID='TR016Z4T4'
count=1
execute_curl "PDH_Circuit" "TR016Z4T4" $count ; count=$((count+1))
execute_curl "CellSite" "0411M" $count; count=$((count+1))
execute_curl "CellSite" "6512M" $count; count=$((count+1)) 
execute_curl "HubSite" "8359M" $count; count=$((count+1))

execute_curl PDH_Circuit TR003D7NK  $count ; count=$((count+1))
execute_curl HubSite 0850W $count ; count=$((count+1))
execute_curl CellSite 1802W $count ; count=$((count+1))
execute_curl CellSite 9502W $count ; count=$((count+1))
execute_curl HubSite F171W $count ; count=$((count+1))


execute_curl PDH_Circuit TR002A16S    $count ; count=$((count+1))
execute_curl CellSite 0782B  $count ; count=$((count+1))
execute_curl CellSite 0787B  $count ; count=$((count+1))
execute_curl CellSite 0962B  $count ; count=$((count+1))
execute_curl CellSite 0963B   $count ; count=$((count+1))
execute_curl HubSite 5297B  $count ; count=$((count+1))


 execute_curl PDH_Circuit TR00317LA $count ; count=$((count+1))
 execute_curl CellSite 0805S $count ; count=$((count+1))
 execute_curl CellSite 5119S $count ; count=$((count+1))


 execute_curl IP_Consumer TR00001PD $count ; count=$((count+1))
execute_curl CellSite 5186D $count ; count=$((count+1))
execute_curl CellSite 9530D $count ; count=$((count+1))

execute_curl IP_Consumer TR00002TR  $count ; count=$((count+1))
execute_curl HubSite 0829S  $count ; count=$((count+1))
execute_curl HubSite 2940S  $count ; count=$((count+1))

execute_curl IP_Consumer TR000055I  $count ; count=$((count+1))
execute_curl CellSite 0081B  $count ; count=$((count+1))

execute_curl IP_Consumer TR00000IU $count ; count=$((count+1))
execute_curl CellSite 7884M $count ; count=$((count+1))


execute_curl IP_Consumer TR00000P8  $count ; count=$((count+1))
execute_curl  CellSite 6994M $count ; count=$((count+1))
execute_curl  CellSite 7350M $count ; count=$((count+1))

mkdir subgraphs
mv subgraph*.json subgraphs/

echo COMMAND IS:
echo "java -jar  igor-5.0.0-SNAPSHOT-jar-with-dependencies.jar --out-directives directives/generated/ --out-red generated/ --in-visualization directives/specifications/ui.js --out-other other/ $folder_path"
echo "java -jar  igor-5.0.0-SNAPSHOT-jar-with-dependencies.jar --out-directives directives/generated/ --out-red generated/ --in-visualization directives/specifications/ui.js --out-other other/ $folder_path" > igor_upload_command.sh 


echo "Remember to update sim files, edit ui.js to remove unnecessary entities, and copy over security.js"




