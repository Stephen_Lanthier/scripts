import os
import re
import json
import string
import random
from shutil import copyfile
from glob import glob
from faker import Faker
from faker.providers import internet
import codecs

debug=1 
#sudo -H pip install Faker
 
# HEY YOU!! 
# This is list of fields we are looking for to anonymize....
# Go through each field of your IGOR json files and find what contains sensitive data...
# You will want to edit for each project based on what Igor generates

#listOfFields = ["holder", "displayName", "siteId", "siteName", "postalCode", "city", "address", "equipmentId", "equipmentName", "ipAddress", "access", "region", "site", "networkElement", "net_ipAddress", "simLatitude", "simLongitude" ]
listOfFields = ["holder", "objectName", "cardName", "systemDescription" ]

# ALSO !
# IF you have specific needs for type of data -> e.g. address should be a random address instead of random string, 
# see the if statement that starts with CUSTOMIZE ME!

#What about model, circuitName, vendor, equipmentType, trailName, region
#fieldNames is fields (json headers) specific for each file
fieldNames = []
fake = Faker()


def generate_address():
    return fake.street_address()

def generate_fullname():
    name = fake.first_name() + " " + fake.last_name()
    return name

def generate_phone():
    return fake.phone_number()

def generate_msisdn():
    return fake.msisdn()

def generate_city():
    return fake.city()

def generate_country():
    return fake.country()

def generate_zipcode():
    return fake.zipcode_in_state()

def generate_ipv4():
    return fake.ipv4_private()

def generate_latitude():
    return fake.latitude()

def generate_sentence(numWords):
    return fake.sentence(nb_words=numWords, variable_nb_words=True, ext_word_list=None)

def generate_longitude():
    return fake.longitude()

def generate_company():
    return fake.company()

def generate_clli():
    clli=''.join(random.choice(string.ascii_uppercase + string.digits) for _ in range(8))
    return clli

def generate_email():
    email=fake.ascii_email(*args, **kwargs)
    return email 

def generate_date():
    return fake.date(pattern='%Y-%m-%d', end_datetime=None)

def anonymize_file():
    # pass file as input parameter. requires filename and field to anonymize
    return

def get_field_number():
    #
    return 

def get_longitude():
    return fake.longitude()

def get_latitude():
    return fake.latitude()

def get_company():
    return fake.company()


def get_fields(file):
    # returns a list of fields (header row) from file as json
    fieldList = []
    with open(file) as jsonfile:
        if debug : print("DEBUG [get_fields]: jsonfile " + file)   
        reader = json.loads(jsonfile.read())
        #FIXME - this loads as python dictionary
        jsondump = json.dumps(reader)
        if debug : print(jsondump)
        #if debug : print("DEBUG [get_fields]: reader " + json.dumps(reader))
        for field in reader:
            if debug : print("DEBUG [get_fields]: Found field " + field)
            fieldList.append(field)
    jsonfile.close()
    return fieldList

# Return all string values from file "file" with json fieldName "fieldName"
def get_all_values(file,fieldName):
    allValues = []
    if (os.stat(file).st_size == 0):
         if debug : print("DEBUG[get_all_values] file " + file + " is empty, skipping")
    else:
        with open(file,'r') as jsonfile:
                reader = json.loads(jsonfile.read()) #FIXME - loads as dictionary
                # is fieldName in reader.fieldnames ? if not, skip file
                #print("DEBUG fieldName is " + fieldName + " File is " + file )
                #FIXME:
                #if fieldName in reader.fieldnames:
                if fieldName in reader:
                    for row in reader:
                        if debug : print("DEBUG [get_all_values]: Value of " + fieldName + " in file " + file + ":" + row[fieldName])
                        allValues.append(row[fieldName])
    return allValues


def get_files_in_cwd():
    jsonFiles = [y for x in os.walk(".") for y in glob(os.path.join(x[0], '*.json'))]
    for file in jsonFiles:
         if debug : print("DEBUG[get_files_in_cwd]: file is " + file)
    return jsonFiles

def backup_file(file):
    # backup each file in folder
    pattern = re.compile(r'json', re.IGNORECASE)
    newFile = pattern.sub('bak', file)
    if os.path.exists(newFile):
        if debug : print("DEBUG [backup_file]: Destination file exists! Not overwriting! " + newFile)
    else:
        if debug : print("DEBUG [backup_file]: Copy old file " + file + " newfile: ." + newFile)
        #copyfile(file, "." + newFile)
        copyfile(file, newFile)
    return

#This function removes non-ascii characters from strings
def removeNonAscii(s): 
    return "".join(i for i in s if ord(i)<128)

#iteratively go through json files and remove non-ascii characters that will trip us up later
def fix_ascii(file):
    newfile=file + ".fixed"
    s = open(file,"r+")
    w = codecs.open(newfile,"w+", encoding='utf8')
    for line in s.readlines():
        line=removeNonAscii(line)
        w.write(line)
    s.close()
    w.close()
    copyfile(newfile,file)

def fix_file(file,string,newstring):
    #print("DEBUG [fix_file] : input file is " + file + " input string is " + string + " change to : " + newstring)   
    newfile=file + ".new"
    s = codecs.open(file,"r", encoding='utf8')
    w = codecs.open(newfile,"w+", encoding='utf8')
    for line in s.readlines():
        #removeNonAscii(line)
        newline=line.replace(string,newstring)
        if debug : 
            if (newline != line): print("DEBUG [fix_file]: File " + file + " fixed line is  "+newline)   
        w.write(newline)
    s.close()
    w.close()
    copyfile(newfile,file)

def existsInFile(file,string):
    with open(file,"r") as myfile:
     if string in myfile.read():
         if debug : print('DEBUG[existsInFile] string ' + string + " found in file " + file)
         return 1
    return 0
####MAIN###################################################

# Store list of json files in a list
jsonFilesList = get_files_in_cwd()
for file in jsonFilesList:
    backup_file(file)
    fix_ascii(file)

output=open('commands.sh','w+')

#updateDict = dict() #dictionary of old>new values
# Create your dictionary class 
class myDict(dict): 
  
    # __init__ function 
    def __init__(self): 
        self = dict() 
          
    # Function to add key:value 
    def add(self, key, value): 
        if key in self.keys():
            print("key " + key + " already exists")
        else:    
            self[key] = value 

updateDict = myDict() #dictionary of old>new values
   
#This main loop builds a list of oldvalues > newvalues and stores in a dictionary (updateDict)
for file in jsonFilesList:
    if debug : print("DEBUG [main]: ************************ Processing/analyzing file " + file)
    if (os.stat(file).st_size == 0):
        if debug : print("DEBUG [main]: File " + file + " is empty. Skipping analysis.")
        continue
    fieldNames = get_fields(file)
    for field in fieldNames:   
        # Only look for field if it's in our target 
        if debug : print ("DEBUG[main]: Checking if field " + field + " is in list of fields we care about" )
        if field in listOfFields:
            if debug : print ("DEBUG[main]: field " + field + " *IS* in list of fields we care about" )
            #currentValues is list of all possible values for current field
            
            #Only replace the value if it ISN'T pre-pended with an undercore
            currentValues = []
            #FIXME:
            currentValues.extend(get_all_values(file,field))
            #Now iterate through the files (nested) again to find if this field exists 
            myuniqueset = list(set(currentValues))
#            raw_input("Press Enter to continue...")  
            
            #item is all current values from current source file of field searched for

            for item in myuniqueset:
                if debug : print ("DEBUG[main]: Finding replacement for item " + item )
                #newitem=''.join(random.choice(string.ascii_uppercase + string.digits) for _ in range(8))
                # CUSTOMIZE ME!! Based on what your json attribute fields are and what data you'd like to generate
                if (field == "address"):
                    newitem=generate_address()
                #elif (field == "ipAddress"):
                    #newitem=generate_ipv4()
                elif (field == "FixPhone"):
                    newitem=generate_phone()
                elif (field == "MobPhone"):
                    newitem=generate_phone()
                elif (field == "Name"):
                    newitem=generate_fullname()                    
                elif (field == "Manufacturer"):
                    newitem=generate_company()
                elif (field == "simLongitude"):
                    newitem=generate_longitude()
                elif (field == "simLatitude"):
                    newitem=generate_latitude()                    
                elif (field == ""):
                    if debug : print "DEBUG[main]: blank field found"
                elif (field == " "):
                    if debug : print "DEBUG[main]: blank \" \" field found"
                else:
                    #displayName, siteId, site, networkElement:
                    newitem=generate_clli()                    
                #We need to iterate one more time!
                #This crazy 3 lines to remove anything after a slash, so we don't mess up sed command TODO: move to def
                pattern=re.compile(r'/') 
                templist=pattern.split(item,0)
                item=templist[0]
                #Better if we replace dot "." with "\." TODO: move to def
                pattern=re.compile(r'\.')
                item=pattern.sub('\.',item,3)
                # need to make sure item isn't blank or whitespace or just . or ? or *
                #output.write("sed -i \'s/" + item + "/" + newitem +"/g\' " + " $file1 " + " > " + " $file2 " + "\n" )
                #TODO don't add duplicate if item already exists
                if ((item == "." ) or ( item == "?" ) or ( item == "*" ) or ( item == ".*")):
                    if debug : print("DEBUG [main]: wildcard found" + item )
                elif (len(item) < 3):
                    if debug : print("DEBUG [main] string " + item + " too short to change")
                elif (item and item.strip()):
                    updateDict.add(item, newitem)
                else:
                    if debug : print("DEBUG [main] blank key found in file " + file + " for field " + field)


 #DEBUG ONLY iterate through values
if debug: 
    for key in updateDict:
        print("DEBUG[main 2nd loop] updateDict old value is: " + str(key) + " new value is " + str(updateDict[key]))

#Here's where we check our files for the keys we generated - if it exists, replace in spot.
raw_input("About to edit files. Press Enter to continue...")
for key in updateDict:
    for file in jsonFilesList:
        if existsInFile(file,key):
            if debug: print("DEBUG[main 3rd loop] found " + key + " in file " + file + " changing to " + updateDict[key])
            fix_file(file,key,updateDict[key])

print("All done!")
