# Anonymizer for Igor

This repo contains script that 
1. Retrieves services in Igor format from a non-obfuscated environment.
2. Prepares data for re-ingest by Igor
3. Anonymizes the data as per some basic configuration
4. Creates the JSON and custdoc files (via Igor) to re-ingest into a "clean" deployment

----

[TOC]

----

## Purpose

The purpose of this tool is to 
*retrieve a set of services and their subservices via Igor from running system (e.g. customer-data environment) , 
*anonymize/obfuscate the data based on some preliminary configuration by user,
*create necesary files to re-ingest data into a new environment.

## Scripts

#### **01_generate_subgraphs.sh** : this is a script that generates subgraphs. You need to edit this file manually to choose the services and sub-services you want to grab. 
	This script generates subfolders (e.g 01, 02, 03) for each service and subservice, containing Igor CSV Files. sim_types.csv files are merged into 1 file, and placed in the 01 folder.
	More detail below on how to configure this script.
#### **02_anonymizer.py** : this script anonymizes CSV data based on configured list of column headers. Once it finds data to anonymize (e.g. displayName), it will look for that value in other CSV files (in all subfolders)
  and make those changes consistently. It will also search for string in substrings. E.g. if we are changing value of Port1, instances where a column is Port1_to_Port2, the string for Port1 is also changed within the larger string.
  Types of anonymization are customizable. E.g. we can tell script to anonymize a certain column to use fake clli's, cities, addresses, email addresses, or a person's name.
#### **03_igor_upload_command.sh**: this script creates the JSON files taken by the Igor re-synch processor to re-ingest into a clean system. The JSON files must be uploaded to Nifi following the Igor instructions.
#### **restore_backups.py**: more for testing, this restores the backups of CSV files created by 02_anonymizer

## Planning

Choose a representation of Services you want to model. You need to get the "type" of service (as known by the SIM), and \_id (hopefully this is available in your vizualization, else you will have to find it the hard way).
Browse in the UI for any related services, and grab those too.
Edit the 
 
### Edit 01 script:
Change any hostnames of server to the server you are grabbing services from. (E.g. change ps02.cenx.localnet to whatever)
The script will grab a token needed for curl RESTAPI commands.
The anchors and what to display are hardcoded in script. If you want to add different anchors, or change what field is displayed in GUI, change this string:
```
sed -e 's/,SIMContainer,,,,,/,SIMContainer,,displayName,,,/g' -e 's/,SIMSNC,,,,,/,SIMSNC,,displayName,TRUE,,/g' -e 's/,SIMLTP,,,,,/,SIMLTP,,displayName,,,/' -e 's/,SIMLinkCon,,,,,/,SIMLinkCon,,displayName,,,/' sim_types.bak > sim_types.csv
Change these lines:
egrep -e 'displayName|TR016Z4T4|TR003D7NK|TR002A16S|TR00317LA' PDH_Circuit.csv > PDH_Circuit.new
egrep -e 'displayName|TR00001PD|TR00002TR|TR000055I|TR00000IU|TR00000P8' IP_Consumer.csv > IP_Consumer.new
```
to only include the parent services you want to include. Otherwise, the child services you requested will add ALL parent services, which means you may get more parent services than you wanted (and they'll be incomplete).
The file name should be whatever the CSV file corresponds to the parent services (SIM type) - typically SIMSNCs.

Change these sections:
```
execute_curl "PDH_Circuit" "TR016Z4T4" $count ; count=$((count+1))
execute_curl "CellSite" "0411M" $count; count=$((count+1))
execute_curl "CellSite" "6512M" $count; count=$((count+1)) 
execute_curl "HubSite" "8359M" $count; count=$((count+1))
```

The first line is the parent service, and the next 3 lines are the child services that we discovered in the UI. Note we took the PDH_Circuit value of TR016Z4T$ and appended it to the "sed" command above.
PDH_Circuit, CellSite, HubSite are the service types, as defined by derived SIMs. The next field (TR016Z4T$, 0411M, 6512M) are the \_id values, which don't necessary correspond to the display names.
You can get this value by looking at data in Cassandra or Solr (e.g. look at trail details in parent service). 
This is not necessarily intuitive - consult with SI's and SA's for your project to understand how to get this data, or customize the ui.js to show the \_id.

The rest of the script doesn't need to be edited.
The script will create the subgraphs, folders for each service, sim_types.csv for each service (but merge the sim_types.csv into 1 to reduce Igor errors), fix anchors and display names, 
then finally create the script for redeploying the services to a new server.

### Edit 02 script
There is a list of fields that you want to anonymize at top of script. Look at your generated CSV files to find headers you want to modify. This may be an iterative process (you may find more fields you want to anonymize once you get your data on a server with UI)

```
listOfFields = ["holder", "displayName", "siteId", "siteName", "postalCode", "city", "address", "equipmentId", "equipmentName", "ipAddress", "access", "region", "site", "networkElement", "net_ipAddress", "simLatitude", "simLongitude" ]
```

You can customize what type of data gets generated for each field. If you don't choose a field, a clli-like value is generated.

Add to or change the *if* statement below. The possible types of "fake" data can be found in the names of the procedures above 
(e.g. generate_address, generate_ipv4, generate_company, generate_city, generate_sentence(# words), so on)

``` 
if (field == "address"):
                    newitem=generate_address()
                elif (field == "ipAddress"):
                    newitem=generate_ipv4()
                elif (field == "net_ipAddress"):
                    newitem=generate_ipv4()
                elif (field == "company"):
                    newitem=generate_company()
                elif (field == "postalCode"):
                    newitem=generate_zipcode()
                elif (field == "city"):
                    newitem=generate_city()
                elif (field == "access"):
                    newitem=generate_sentence(8)
                elif (field == "siteName"):
                    newitem=generate_city()
                elif (field == "simLongitude"):
                    newitem=generate_longitude()
                elif (field == "simLatitude"):
                    newitem=generate_latitude()                    
                elif (field == ""):
```

## Running the scripts

TO DO
 
### Example of script execution

TO DO

