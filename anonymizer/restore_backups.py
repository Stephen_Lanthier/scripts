import os
import re
import csv
import string
import random
from shutil import copyfile
from glob import glob


def restore_files_in_cwd():
    csvFiles = [y for x in os.walk(".") for y in glob(os.path.join(x[0], '*.bak'))]
    for file in csvFiles:
        print file
        newfile=file.replace('.bak','.csv')
        print("DEBUG rename " + file + " newfile: " + newfile)
        copyfile(file,newfile)
    return csvFiles

#def backup_file(file):
    # backup each file in folder
    #pattern = re.compile(r'csv', re.IGNORECASE)
    #newFile = pattern.sub('bak', file)
    #if os.path.exists(newFile):
        #print("Destination file exists! Not overwriting! " + newFile)
    #else:
        #print("Copy old file " + file + " newfile: ." + newFile)
        ##copyfile(file, "." + newFile)
        #copyfile(file, newFile)
    #return

# Store list of CSV files in a list
csvFilesList = restore_files_in_cwd()
#for file in csvFilesList:
    #backup_file(file)

