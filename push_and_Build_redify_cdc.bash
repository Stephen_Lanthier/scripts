#!/bin/bash

deployment_folder="env-osp"
development_folder="osp"
append=`date +%d_%m_%y`
HOST='localhost'
neopwd='trainer' # your neo4j password, if you have one....
force='false'

if [ "$1" == "-f" ]; then
	force='true'
	echo "Responding yes to all prompts"
fi
#pushd "$development_folder/nifi/js/"
if [ "$force" !=  "true" ]; then
	echo "Update Redify? (y/n)"
	read answer
	echo $answer | grep -i "y" 
fi
if  [ $? == 0 ]; then
	pushd ~/$development_folder//nifi/js/
	npm run generate
	if  [ $? == 0 ]; then
		cp ~/$deployment_folder/target/nifi/data/resources/redify_greenbox.js ~/$deployment_folder/target/nifi/data/resources/redify_greenbox.js.$append.bak
		cp ../resources/redify_greenbox.js ~/$deployment_folder/target/nifi/data/resources/redify_greenbox.js
	else
		echo Error building redify
		exit 1
	fi
	popd
fi

if [ "$force" !=  "true" ]; then
	echo "Deploy via CDC? (ensure your cdc config is correctly configured) (y/n)"
	read answer
	echo $answer | grep -i "y" 
fi
if  [ $? == 0 ]; then
		cat ~/.cenx/cdc.json |grep rootUrl
		if [ "$force" !=  "true" ]; then
			echo "Is this the correct server? Enter to proceed, ctrl-C to quit"
			read answer
		fi
		pushd ~/$development_folder/
		cdc compile source/directives directives.$append.json
		#echo "DEBUG return result: " $?
		if  [ $? == 0 ]; then
			cdc deploy directives.$append.json
		else
			echo Error deploying directives. Check errors and ares logs.
		exit 1
		fi
		popd
fi

if [ "$force" !=  "true" ]; then
	echo "Clear Cassandra tables? (y/n)"
	read answer
	echo $answer | grep -i "y" 
fi
if  [ $? == 0 ]; then
	cqlsh $HOST -e "truncate devcenx.entity;"
	cqlsh $HOST -e "truncate devcenx.link;"
fi

if [ "$force" !=  "true" ]; then
	echo "Restart ares? (localhost supported only) (y/n)"
	read answer
	echo $answer | grep -i "y" 
fi
if  [ $? == 0 ]; then
	echo "restarting ares hold on...."
	pushd ~/$deployment_folder/
	orca service restart ares
	popd
fi

if [ "$force" !=  "true" ]; then
	echo "Clear Neo4j? (localhost supported only) (y/n)"
	read answer
	echo $answer | grep -i "y" 
fi
if  [ $? == 0 ]; then
	# get cypher-shell via brew install cypher-shell
	cypher-shell -u neo4j -p $neopwd 'MATCH (n) DETACH DELETE  n'
fi

#TO DO - start and stop nifi processors to re-ingest data
# token=$(curl -k 'https://localhost:9443/nifi-api/access/token'  --data 'username=ikent&password=tester’)

# curl 'https://<nifi-node>:9091/nifi-api/processors/aab961c3-6bcd-18e7-0000-00001d74d4ea' -X PUT -H "Authorization: Bearer $token" -H 'Content-Type: application/json' -H 'Accept: application/json, text/javascript, */*; q=0.01'  --data-binary '{"revision":{"clientId":"248a328f-a133-1cd0-18c1-8997e36ef898","version":1},"component":{"id":"aab961c3-6bcd-18e7-0000-00001d74d4ea","state":"STOPPED"}}' --compressed --insecure


if [ "$force" !=  "true" ]; then
	echo "Clear Solr? (localhost support only) (y/n)"
	read answer
	echo $answer | grep -i "y" 
fi
if  [ $? == 0 ]; then
	#lets brute force clear all solr
	curl 'http://localhost:8983/solr/parker1/update?stream.body=<delete><query>*:*</query></delete>&commit=true'
	curl 'http://localhost:8983/solr/parker2/update?stream.body=<delete><query>*:*</query></delete>&commit=true'
	curl 'http://localhost:8983/solr/parker3/update?stream.body=<delete><query>*:*</query></delete>&commit=true'
	curl 'http://localhost:8983/solr/parker4/update?stream.body=<delete><query>*:*</query></delete>&commit=true'

fi

