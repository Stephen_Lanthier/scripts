#!/bin/sh

cd /cenx/ORCA/training
export ORCA_OFFLINE_MODE=true

cp /cenx/ORCA/training/deploy.yaml_template /cenx/ORCA/training/deploy.yaml

rtools01=$(getent hosts centos1 | awk '{print $1}')
rsystem01=$(getent hosts centos2 | awk '{print $1}')

sed -i "s/centos1/$rtools01/" /cenx/ORCA/training/deploy.yaml
sed -i "s/centos2/$rsystem01/" /cenx/ORCA/training/deploy.yaml

# fix permissions on system - requires sshpass here and sudoers NOPASSWD on system
sshpass -p 'ready!Tech' ssh -o StrictHostKeyChecking=no deployer@system 'sudo mkdir -p /cenx/nifi/data/nars'
sshpass -p 'ready!Tech' ssh -o StrictHostKeyChecking=no deployer@system 'sudo chown  deployer:docker   /cenx/nifi/data/nars'

/cenx/bin/orca stack update
/cenx/bin/orca service start
/cenx/bin/orca app update
/cenx/bin/orca app start
/cenx/bin/orca stack status
