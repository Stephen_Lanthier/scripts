#This will search data files (CSV files) for search term such as backhaul service
# and created new csv files with just those matching entries
# author: s lanthier

# TO DO: let's pass the new folder name via input parameters

import os
import re
import sys
import datetime

x = datetime.datetime.now() 

new_file_extension = x.strftime("%m_%d_%Y")

path = '.'
#new_file_extension = 'smaller'
search_strings = []
#search_strings.append("ID")

if not os.access(new_file_extension, os.R_OK):
    os.mkdir(new_file_extension)
else:
        print("Target directory " + new_file_extension + " exists. Remove before proceeding.\n")
        exit()

if len(sys.argv) == 1:
    print("Usage: " + sys.argv[0] + "<search terms>")
    print("This script creates smaller ingest data files from larger ones based on search terms.")
    print("New files will have extension of MM DD YYYY")
    exit()

for eachArg in sys.argv:   
        print(eachArg)
        search_strings.append(eachArg)

files = []
# r=root, d=directories, f = files
#for r, d, f in os.walk(path):
for r,d,f in os.walk(path):
    for file in f:
        if '.csv' in file:
            files.append(os.path.join(r, file))

for f in files:
    #print(f)
    mySearchStrings = search_strings[:]
    newfile =  f.replace(".csv", "_" + new_file_extension + ".csv" )
    print("Writing file: " + new_file_extension + "/" + newfile)
    fo = open(new_file_extension + "/" + newfile,'w')
    fr = open(f, 'r')
    first_line = fr.readline()
    fo.write(first_line)
    fr.close()
    for search_term in mySearchStrings:
        for line in open(f, 'r'):
            if re.search(search_term, line):
                # debug only : 
                #print(line)
                fo.write(line)
    fo.close()
