#This will rename CSV files to strip any temporary suffix

# author: s lanthier

import os
import re
import sys
from shutil import copyfile

path = '.'


if len(sys.argv) != 2:
    print("Usage: " + sys.argv[0] + "<target_directory>")
    print("This script will remove suffixes from data files and copy files to target directory")
    print("The suffix is expected to be _mm_dd_yyyy.csv format")
    #TO DO allow for multiple hyphen - or underscore _ in suffix
    exit()

target_directory = sys.argv[1]
if not os.access(target_directory, os.W_OK):
        print("Target directory " + target_directory + " does not exist or not writeable.\n")
        response = raw_input("Do you want to create (y/n)? ") 
        if response == "y":
            os.mkdir(target_directory)
        else:
            print "Ok - exiting!"
            exit()
files = []
# r=root, d=directories, f = files
 
for r,d,f in os.walk(path):
    for file in f:
        if '.csv' in file:
            files.append(os.path.join(r, file))

for f in files:
    #print("old file name: " + f)
    newfile =  re.sub("[_0-9]+\.csv",  ".csv" , f)
    newfile = re.sub("_smaller_","_",newfile)   #strip _smaller if it exists 
    newfile_full = target_directory + "/" + newfile
    #print("new file name:    " + newfile)
    #print("new file full name:    " + newfile_full)
    copyfile(f,newfile_full)