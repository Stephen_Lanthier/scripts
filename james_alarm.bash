
#sed -e "s/1566299750785/`date +%s`/" james_alarm.bash > new_james_alarm.bash ; bash new_james_alarm.bash

curl -i -d '{
   "transactionId":"3a79bf8b-b838-48a0-a241-944f6eba540d",
   "timestamp":1566299750785,
   "nmsType":"OneFM",
   "nmsId":"OneFM-MBH",
   "alarmEvents":[
      {
         "EXTENDEDATTR":"",
         "RECEIVEDATPROBE":"2019-08-20T11:15:35.000Z",
         "NODESTATUS":"",
         "X733PROBABLECAUSE":"Antenna Problem (GSM 12.11)",
         "SERVICEAFFECTING":"No",
         "FDN":"",
         "PHYSICALSLOT":"0",
         "SUMMARY":"Steve testing please ignore",
         "PHYSICALSHELF":"0",
         "TTCREATIONTIME":"1970-01-01T00:00:00.000Z",
         "PHYSICALCARD":"",
         "STATECHANGE":"2019-08-20T11:15:46.000Z",
         "CUSTOMER":"Orange-OF-RO",
         "NODETYPE":"Charging and Billing Servers - CCN",
         "MANAGER":"ericsson-etiger-jdbc@172.23.159.39:6791",
         "NODEALIAS":"172.29.231.202",
         "ACKNOWLEDGETIME":"1970-01-01T00:00:00.000Z",
         "SERVICE":"testservice",
         "IDENTIFIER":"ACTtest2008",
         "CLEARTIME":"2019-08-22T11:15:35.000Z",
         "PHYSICALPORT":"0",
         "SITENAME":"",
         "LASTOCCURRENCE":"2019-08-20T11:15:35.000Z",
         "NODE":"BALX0036",
         "EVENTID":"",
         "ALERTKEY":"\/rack=1\/shelf=3\/slot=3",
         "SITEID":"BTS8",
         "DEVICEMODEL":"abcd",
         "OBJECT":"PhysicalName=LPU 7 FaultCause=Board get Elabel failed",
         "X733SPECIFICPROB":"Local Cell Unusable",
         "ALERTGROUP":"Probe Heartbeat",
         "REGION":"North",
         "SERVERSERIAL":"AGG_L2:12440214",
         "ADDITIONALSTRING":"",
         "TT_FLAG":"Not Defined",
         "X733EVENTTYPE":"AttributeValueChange",
         "NODEIP":"",
         "FIRSTOCCURRENCE":"2019-08-20T11:15:35.000Z",
         "TT_ID":"",
         "ACKNOWLEDGER":"",
         "SEVERITY":"Major"
      }
   ]
}' -H "Content-Type: application/json" -H "Accept: application/json" -X POST http://orange-spain-project.cenx.localnet:8080/onefm/collectors/api/v1/alarm/
