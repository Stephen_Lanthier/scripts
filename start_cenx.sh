#!/bin/bash
# This script is used by cenx.service on server reboot/start.

/usr/local/bin/orca --offline service start
if [ $? -eq 0 ]; then
   echo OK
else
   echo RETRYING
   /usr/local/bin/orca service start
fi

sleep 10

/usr/local/bin/orca --offline service start
if [ $? -eq 0 ]; then
   echo OK
else
   echo RETRYING
   /usr/local/bin/orca service start
fi

sleep 10 

/usr/local/bin/orca --offline app start
if [ $? -eq 0 ]; then
   echo OK
else
   echo RETRYING
   /usr/local/bin/orca app start
fi

sleep 10

/usr/local/bin/orca --offline app start
if [ $? -eq 0 ]; then
   echo OK
else
   echo RETRYING
   /usr/local/bin/orca app start
fi

sleep 30

#cd /cenx/xfer/deployment_tools_and_data; bash audit_deploy.sh tools01




