#!/bin/bash
# Generates git add statements for each modified file in your git workspace. 
# You probably don't want to cut and paste each line - be selective.

for line in `git status |egrep -e 'modified|new file' | cut -d: -f2 `
do
	echo  git add $line
done
