#!/bin/sh
docker exec -it neo4j sh -c  'rm /data/dbms/auth'
docker exec -it neo4j sh -c '/var/lib/neo4j/bin/neo4j-admin set-initial-password neo4j'
docker exec -it neo4j sh -c '/var/lib/neo4j/bin/neo4j restart'