#!/bin/bash
if [ $# -ne 1 ]
then
	  echo "Syntax: $0 <container id>"
	  echo "e.g. docker ps |grep zookeeper"
	  echo "e.g. docker ps |grep kafka"
	  echo "docker inspect -f '{{range .NetworkSettings.Networks}}{{.IPAddress}}{{end}}' id_for_zookeeper "
	  exit 0
fi
 
docker inspect -f '{{range .NetworkSettings.Networks}}{{.IPAddress}}{{end}}' $1
