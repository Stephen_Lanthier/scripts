#!/bin/bash

deployment_folder="/cenx/ORCA/training"
append=`date +%d_%m_%y`

CASSANDRA_HOST='system01'
CASSANDRA_KEY='devcenx'
SOLR_HOST='system01'
NEO4J_HOST='127.0.0.1'

neopwd='trainer' # your neo4j password, if you have one....
force='false'


if [ ! -f $deployment_folder"/deploy.yaml" ]; then
	echo "Deployment folder $deployment_folder missing or missing deploy.yaml. Aborting"
	exit 1
fi


if [ "$1" == "-f" ]; then
	force='true'
	echo "Responding yes to all prompts"
else
	echo ""
	echo "Your deployment folder is $deployment_folder"
	echo "Your cassandra hostname or IP is $CASSANDRA_HOST"
	echo "It should be " `grep cassandra_ip $deployment_folder/deploy.yaml`
	echo "Your cassandra key is $CASSANDRA_KEY"
	echo "It should be " `grep cassandra_prefix $deployment_folder/deploy.yaml` " + cenx "
	echo "Your solr host is $SOLR_HOST"
	echo "It should be where ares is running (typically system01 - see 'machines' section of your deploy.yaml)"
	if [ -f /usr/bin/cqlsh ]; then
		echo "cqlsh is installed"
	else
		echo "cqlsh is not installed - clearing of Cassandra tables will not take place"
	fi
	echo ""
	echo "If this is not correct, ctrl-c to quit and edit variables at top of this script."
	echo "Else hit enter to proceed..."
	read answer
fi

##############################################################################
if [ "$force" !=  "true" ]; then
	echo "Clear Cassandra tables? (y/n)"
	read answer
	echo $answer | grep -i "y" 
fi
if  [ $? == 0 ]; then

	if [ -f /usr/bin/cqlsh ]
	then
		# need to hard code the cql version as only 1 version is supported. TODO : Make this a variable
		cqlsh --cqlversion=3.4.0 $CASSANDRA_HOST -k $CASSANDRA_KEY -e "truncate entity;"
		cqlsh --cqlversion=3.4.0 $CASSANDRA_HOST -k $CASSANDRA_KEY -e "truncate link;"
	else
		echo "cqlsh not found. Run the following commands to install:"
		echo sudo yum install https://dl.fedoraproject.org/pub/epel/epel-release-latest-7.noarch.rpm
		echo sudo yum install gcc
		echo sudo yum install python-pip
		echo sudo pip install cqlsh
		echo 'echo export CQLSH_NO_BUNDLED=true >> ~/.bashrc'
		echo 'source ~/.bashrc'
		echo "\nAttempting to run for you... sudo might fail ... Hit enter to proceed, or ctrl-C to try yourself. "
		read answer
		sudo yum install -y https://dl.fedoraproject.org/pub/epel/epel-release-latest-7.noarch.rpm
		sudo yum install -y gcc
		sudo yum install -y python-pip
		sudo pip install -y cqlsh
		echo export CQLSH_NO_BUNDLED=true >> ~/.bashrc
		source ~/.bashrc
		cqlsh --cqlversion=3.4.0 $CASSANDRA_HOST -k $CASSANDRA_KEY -e "truncate entity;"
		cqlsh --cqlversion=3.4.0 $CASSANDRA_HOST -k $CASSANDRA_KEY -e "truncate link;"
	fi

fi
##############################################################################

if [ "$force" !=  "true" ]; then
	echo "Restart ares?  (y/n)"
	read answer
	echo $answer | grep -i "y" 
fi
if  [ $? == 0 ]; then
	echo "restarting ares hold on...."
	pushd $deployment_folder
	orca service restart ares
	popd
fi
##############################################################################

if [ "$force" !=  "true" ]; then
	echo "Clear Neo4j? (localhost supported only) (y/n)"
	read answer
	echo $answer | grep -i "y" 
fi
if  [ $? == 0 ]; then
	# get cypher-shell via brew install cypher-shell
	echo 'run : "MATCH (n) DETACH DELETE  n" on your neo4j console'
	echo "or\n cypher-shell -a bolt://$NEO4J_HOST:7687 -u neo4j -p $neopwd 'MATCH (n) DETACH DELETE  n'"
	#TODO: actually implement execution of cypher-shell
fi

#TO DO - start and stop nifi processors to re-ingest data
# token=$(curl -k 'https://localhost:9443/nifi-api/access/token'  --data 'username=ikent&password=tester’)

# curl 'https://<nifi-node>:9091/nifi-api/processors/aab961c3-6bcd-18e7-0000-00001d74d4ea' -X PUT -H "Authorization: Bearer $token" -H 'Content-Type: application/json' -H 'Accept: application/json, text/javascript, */*; q=0.01'  --data-binary '{"revision":{"clientId":"248a328f-a133-1cd0-18c1-8997e36ef898","version":1},"component":{"id":"aab961c3-6bcd-18e7-0000-00001d74d4ea","state":"STOPPED"}}' --compressed --insecure


if [ "$force" !=  "true" ]; then
	echo "Clear Solr? (y/n)"
	read answer
	echo $answer | grep -i "y" 
fi
if  [ $? == 0 ]; then
	#lets brute force clear all solr
	echo 'curl http://'$SOLR_HOST':8983/solr/parker1/update?stream.body=<delete><query>*:*</query></delete>&commit=true'
	echo 'curl http://'$SOLR_HOST':8983/solr/parker2/update?stream.body=<delete><query>*:*</query></delete>&commit=true'
	echo 'curl http://'$SOLR_HOST':8983/solr/parker3/update?stream.body=<delete><query>*:*</query></delete>&commit=true'

	curl 'http://'$SOLR_HOST':8983/solr/parker1/update?stream.body=<delete><query>*:*</query></delete>&commit=true'
	curl 'http://'$SOLR_HOST':8983/solr/parker2/update?stream.body=<delete><query>*:*</query></delete>&commit=true'
	curl 'http://'$SOLR_HOST':8983/solr/parker3/update?stream.body=<delete><query>*:*</query></delete>&commit=true'

fi
