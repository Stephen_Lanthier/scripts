# Clean Training Server

This repository contains a tool to clean a training server of ingested data.

----

[TOC]

----


## Purpose

The purpose of this script is to clean all vizualization data (services, RED and SIM entities) from the server before ingesting new data or before use by new students.
This should not be used in a production environment.
This script will:
1. Clean Cassandra data (used for among other things, rebuild of SIM)
2. Clean Neo4J data (if relevant)
3. Restart Ares (force rebuild of graphs and models)
4. Clean Solr (where vizualization data is stored)

## Pre-requisites

### Software:
1. cqlsh: You need to install cqlsh. This is done by installing cassandra on the server you are running the script. 
Follow the following to install cqlsh on Centos:
https://www.liquidweb.com/kb/how-to-install-cassandra-on-centos-7/
To install cqlsh on MacOS (using brew):
brew install cassandra
2. cypher-shell: This only applies if you wish to clear Neo4J from the command line. Currently the script will only provide you with the command to cut and paste for you to run yourself. (Experimental)


### Edit script:
You may need to edit this script (vi) to set the following "Variables" in the script by editing the file:

1. deployment_folder: This is typically /cenx/ORCA/training on training servers, but edit this variable if your ORCA path differs.
2. CASSANDRA_HOST: This is the hostname or IP address of your cassandra server. This is typically system01 or DS. Get this from cassandra_host in your deploy.yaml.
3. SOLR_HOST: The hostname or IP address of your Solr server. Typically system01. Get this from machines section in deploy.yaml.
4. NEO4J_HOST: Not usually deployed. IP address can be anything. Defaults to localhost. This is outside the scope of a Cenx deployment. (Not in deploy.yaml)
5. neopwd: Not usually deployed. (Not fully implemented). The password of your Neo4J server. (Not in deploy.yaml)

Note: it is highly likely the defaults will work for you, but best confirm before running the script


## Running the script

1. When you run the script, the script will prompt you for each component whether you wish to "clean" the data or not. 
E.g. if you want to clean Solr but not Cassandra, you can choose the appropriate responses.
2. The script will test your Cassandra IP address with what it believes your Cassandra IP is from the Orca config (deploy.yaml)
3. The script will check for cqlsh installed. It will attempt to install cqlsh if it is not detected and provide manual steps should you need to do yourself. 
Watch the output for errors.
4. The script will clear the entity and link tables in Cassandra, if you choose "y"es for clearing cassandra
5. The script will restart Ares if you choose to, triggering graph rebuilds and SIM rebuild
6. The script will provide you the command to clear Neo4J. 
Future implementation: will clear it for you, provided you have cypher-shell installed
7. The script will clear Solr, should you choose "y"es for clearing Solr. 
Note: Partitions "parker1" through "parker3" are cleared. If you are using a version of CENX that supports more or less partitions, edit this section accordingly.

### Example of script execution

TO DO

