#sudo -H pip install Faker

from faker import Faker
from faker.providers import internet

fake = Faker()
fake.pybool()   # Randomly returns True/False
print(fake.pyfloat(left_digits=3, right_digits=3, positive=False, min_value=None, max_value=None))   # Float data
print(fake.pystr(min_chars=None, max_chars=10))  # String data
print(fake.pylist(nb_elements=5, variable_nb_elements=True))  # List
fake.add_provider(internet)
print(fake.ipv4_private())  # Fake ipv4 address