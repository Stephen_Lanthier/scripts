#!/bin/bash
if [ ! -f ./config.zk.edn.bak ] ; then
cp /cenx/ORCA/training/target/zookeeper/heimdallr/config.zk.edn ./config.zk.edn.bak
fi
sed -e '/incremental-batch-size/d' /cenx/ORCA/training/target/zookeeper/heimdallr/config.zk.edn > ./config.zk.edn.new
sed -i "/:full/a\ \ \ \ \ \ \ \ \ \ \ \ \ \ :incremental-batch-size 1" ./config.zk.edn.new
sudo cp ./config.zk.edn.new /cenx/ORCA/training/target/zookeeper/heimdallr/config.zk.edn
