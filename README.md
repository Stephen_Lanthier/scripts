# scripts

An assortment of scripts to automate SI and training tasks.


| Script or folder Name   | Purpose       | Comments  |
| ----------------------- |:-------------:| -----:|
| anonymizer/      | Scripts to automate Igor service retrieveal and anonymize Igor output | See below |
| clean_training_server.bash | Cleans cassandra, solr, neo4j to clear Cenx      | N/A  |
| commit_scripts.bash | Just commits scripts in Git      |  Still need git push |
| course_neo4j.bash   | Creates Ne0j instance | N/A |
| create_tunnel.bash  | Creates tunnel to Nifi 9443 using localhost | N/A |
| csv_renamer.py | Just renames Orange CSV file | Don't use | 
| dump_cassandra.bash | Do you want to dump Cassandra DBs to text file? | | 
| extract_data.py | Makes targeted CSV files from larger CSV files | |
| generate_alarm.py | Generates an alarm | |
| get_container_ip.sh | Good for getting Kafka IP address | |
| git_add.bash | Generates commands for adding to git. | Does not actually execute the cmd |
| james_alarm.bash | Alarm script from James for OSP | |
| launch_neo4j.bash | Another script to launch Neo4j | |
| new_james_alarm.bash | Another alarm script | | 
| new_paul_alarm.bash | Another alarm script | | 
| paul_alarm.bash | Another alarm script | | 
| pause_all.bash | Pause your local CENX stack | |
| push_and_Build_redify_cdc.bash | Automate your local CENX for SI development and testing | |
| readytech_start_script.sh | Deploy a readytech server | |
| refresh_ares.bash | localhost version of clean_training_server.bash | |
| rebuild_orca_stack.bash | Does orca rebuild and start | |
| start_cct.bash | Start your local cct instance | |
| stop_orca_stack.bash | Stops your localhost CENX | |
| unpause_all.bash | Opposite of pause_all.bash | |
| watch_ares_log.py | Makes looking at ares logs better | |


Anonymizer
| Script or folder Name   | Purpose       | Comments  |
| ----------------------- |:-------------:| -----:|
