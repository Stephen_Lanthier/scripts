#date +%s 
# to get current epoch
#sed -e "s/1566299750785/`date +%s`/" paul_alarm.bash > new_paul_alarm.bash

curl -i -u osp:osp -k -d '{  
   "transactionId":"31373b-b838-48a0-a241-944f6eba540d",
   "timestamp":1566299750785,
   "nmsType":"OneFM",
   "nmsId":"OneFM-MBH",
   "alarmEvents":[  
      {  
         "X733SPECIFICPROB":"Blah",
         "NODE":"ANDX0006",
         "ALERTKEY":"\/rack=1\/shelf=1\/slot=11\/port=1",
         "SEVERITY":"Major",
         "FIRSTOCCURRENCE":"2019-09-11T09:15:35.000Z",
         "LASTOCCURRENCE":"2019-09-11T09:15:35.000Z",
         "EXTENDEDATTR":"",
         "RECEIVEDATPROBE":"2019-08-20T11:15:35.000Z",
         "NODESTATUS":"",
         "X733PROBABLECAUSE":"Antenna Problem (GSM 12.11)",
         "SERVICEAFFECTING":"No",
         "FDN":"",
         "PHYSICALSLOT":"0",
         "SUMMARY":"Testing please ignore",
         "PHYSICALSHELF":"0",
         "TTCREATIONTIME":"1970-01-01T00:00:00.000Z",
         "PHYSICALCARD":"",
         "STATECHANGE":"2019-08-20T11:15:46.000Z",
         "CUSTOMER":"Orange-OF-RO",
         "NODETYPE":"Charging and Billing Servers - CCN",
         "MANAGER":"ericsson-etiger-jdbc@172.23.159.39:6791",
         "NODEALIAS":"172.29.231.202",
         "ACKNOWLEDGETIME":"1970-01-01T00:00:00.000Z",
         "SERVICE":"testservice",
         "IDENTIFIER":"ACTtest2008",
         "PHYSICALPORT":"0",
         "SITENAME":"",
         "EVENTID":"",
         "SITEID":"BTS8",
         "DEVICEMODEL":"abcd",
         "OBJECT":"PhysicalName=LPU 7 FaultCause=Board get Elabel failed",
         "ALERTGROUP":"Probe Heartbeat",
         "REGION":"North",
         "SERVERSERIAL":"AGG_L2:12440214",
         "ADDITIONALSTRING":"",
         "TT_FLAG":"Not Defined",
         "X733EVENTTYPE":"AttributeValueChange",
         "NODEIP":"",
         "TT_ID":"",
         "ACKNOWLEDGER":""
      }
   ]
}' -H "Content-Type: application/json" -H "Accept: application/json" -X POST http://orange-spain-project.cenx.localnet:8080/onefm/collectors/api/v1/alarm
