#!/bin/bash
# chkconfig: 
# description: Graceful stop/start of orca services 
# Source function library.
# author: stephen.lanthier@ericsson.com

# Create this as root as /etc/init.d/orca
# chmod 755 /etc/init.d/orca
# Create symbolic links as follows
# cd /etc/rc1.d; ln -s ../init.d/orca K10orca
# cd /etc/rc6.d; ln -s ../init.d/orca K10orca
# cd /etc/rc0.d; ln -s ../init.d/orca K10orca

# cd /etc/rc2.d ;ln -s ../init.d/orca S10orca
# cd /etc/rc3.d ;ln -s ../init.d/orca S10orca
# cd /etc/rc4.d ;ln -s ../init.d/orca S10orca
# cd /etc/rc5.d ;ln -s ../init.d/orca S10orca
 

#need this line to prevent startup from using systemctl controls
export SYSTEMCTL_SKIP_REDIRECT=1

#this is standard init.d functions but make sure you skip systemctl
. /etc/init.d/functions


start() {
    su - deployer sh -c "cd /cenx/ORCA/training; /usr/local/bin/orca stack start  "
}

stop() {
    # code to stop app comes here 
    # example: killproc program_name
    su - deployer sh  -c "cd /cenx/ORCA/training; /usr/local/bin/orca stack stop -f "
}

case "$1" in 
    start)
       start
       ;;
    stop)
       stop
       ;;
    restart)
       stop
       start
       ;;
    status)
       su - deployer -c "cd /cenx/ORCA/training; /usr/local/bin/orca status "
       ;;
    *)
       echo "Usage: $0 {start|stop|status|restart}"
esac