#!/bin/sh

echo "Grabbing nifi properties from nifi container..."
if [ ! -f nifi.properties.orig ]
then
	docker cp nifi1:/opt/nifi/conf/nifi.properties nifi.properties.orig
fi

cp nifi.properties.orig nifi.properties.new

#changing these
#nifi.web.http.port=
#nifi.web.http.network.interface.default=
#nifi.web.https.host=0.0.0.0
#nifi.web.https.port=9443
#nifi.web.https.network.interface.default=

echo "Changing nifi properties in local file nifi.properties.new ..."
sed -i 's/^nifi\.web\.http\.port=.*/nifi.web.http.port=7777/g' nifi.properties.new
sed -i 's/^nifi\.web\.http\.host=.*/nifi.web.http.host=/g' nifi.properties.new
sed -i 's/^nifi\.web\.https\.port=.*/nifi.web.https.port=/g' nifi.properties.new
sed -i 's/^nifi\.web\.https\.host=.*/nifi.web.https.host=/g' nifi.properties.new
sed -i 's/^nifi\.web\.proxy\.host=.*/nifi.web.proxy=/g' nifi.properties.new
#nifi.remote.input.secure
sed -i 's/^nifi\.remote\.input\.secure=.*/nifi.remote.input.secure=false/g' nifi.properties.new
echo "Transferring updated nifi.properties file to nifi container ..."
docker cp nifi.properties.new  nifi1:/opt/nifi/conf/nifi.properties 
echo "Restarting nifi. If this takes too long, feel free to Ctrl-C and you should still be okay ..."
docker exec  nifi1 sh -c '/opt/nifi/bin/nifi.sh restart'
echo "Sleeping for 15 seconds before checking if nifi is still running ..."
sleep 15
echo "Checking status of nifi ..."
docker exec nifi1 sh -c '/opt/nifi/bin/nifi.sh status'
# Check /var/log/nifi/nifi-bootstrap.log and /var/log/nifi/nifi-app.log for startup errors if it doesn't start...
echo "Confirm new configuration has kept. Check for secure=false for remote and port 7777 for web."
docker exec  nifi1 sh -c 'cat /opt/nifi/conf/nifi.properties'|grep secure
docker exec  nifi1 sh -c 'cat /opt/nifi/conf/nifi.properties'|grep web