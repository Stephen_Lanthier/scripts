 #!/bin/bash
 
    mkdir -p ~/cct
    pushd ~/cct
    if [ ! -d docker-compose.yml ] ; then
        echo "docker-compose.yml attempting to download with wget"
        wget https://cenx-cf.atlassian.net/wiki/download/attachments/655556684/docker-compose.yml
    fi
    docker-compose up -d
    echo "log in via http://localhost:8030"
    echo "User Guide https://cenx-cf.atlassian.net/wiki/spaces/OL/pages/655556684/User+Guide"
    popd
