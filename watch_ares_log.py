import time
import re

#Change logfile path to your path
logfile = '/cenx//log/ares/ares/ares-logs.log'
#Set debug to 0 under normal operations
debug = 0

class bcolors:
    HEADER = '\033[95m'
    OKBLUE = '\033[94m'
    OKGREEN = '\033[92m'
    WARNING = '\033[93m'
    FAIL = '\033[91m'
    ENDC = '\033[0m'
    BOLD = '\033[1m'
    UNDERLINE = '\033[4m'

fp = open(logfile, 'r')
while True:
        newline = fp.readline()
        # Once all lines are read this just returns ''
        # until the file changes and a new line appears

        if newline:
 
            r0 = re.compile(r'ERROR')
            r1 = re.compile(r'Data Increment 0 processing skipped. Incremental data set was empty|Incremental data set was empty and internal state has stabilized. Batch count will not be incremented')  
            r2 = re.compile(r'Incremental batch processing started: {:batch-count \d, :ingest-events-count 0, :sim-vertices-count 0')      
            r3 = re.compile(r'^\s+\[.*\]$')
            r4 = re.compile(r'Received 0 events from Kafka')
            r5 = re.compile(r'java\.util\.concurrent\.|at org\.apache\.spark\.|at java\.lang\.Thread|at scala\.')
            r6 = re.compile(r'\[ARES-100\]|\[ARES-101\]|\[ARES-102\]|\[ARES-103\]|\[ARES-6002\]|\[ARES-5101\]|SparkListenerBus has already stopped') #some startup messages
            r7 = re.compile(r'\[ARES-602\]') #SIM to SOLR
            r8 = re.compile(r':cause|:via|:type|:message|:at |:trace')  #various java clojure errors
            if r0.search(newline):
                print bcolors.FAIL + bcolors.BOLD + newline + bcolors.ENDC
            elif r1.search(newline):
                if debug : print bcolors.OKBLUE + newline + bcolors.ENDC
            elif r2.search(newline): 
                if debug : print  bcolors.OKBLUE + newline + bcolors.ENDC
            elif r3.search(newline):
                if debug : print  bcolors.WARNING + newline + bcolors.ENDC
            elif r4.search(newline):
                if debug : print  bcolors.OKBLUE + newline + bcolors.ENDC 
            elif r5.search(newline):
                if debug : print bcolors.WARNING + newline + bcolors.ENDC 
            elif r6.search(newline):
                if debug : print  bcolors.OKBLUE + newline + bcolors.ENDC 
            elif r7.search(newline):
                if debug : print  bcolors.OKBLUE + newline + bcolors.ENDC      
            elif r8.search(newline):
                if debug : print  bcolors.WARNING + newline + bcolors.ENDC             
            else: 
                #if debug : print "DEBUG good log entry found: "
                print bcolors.OKGREEN + newline + bcolors.ENDC
else:
     time.sleep(0.5) 

 
