import time
import re

#Change logfile path to your path
logfile = '/cenx//log/wildfly/wildfly_dc1/server.log'
#Set debug to 0 under normal operations
debug = 0

class bcolors:
    HEADER = '\033[95m'
    OKBLUE = '\033[94m'
    OKGREEN = '\033[92m'
    WARNING = '\033[93m'
    FAIL = '\033[91m'
    ENDC = '\033[0m'
    BOLD = '\033[1m'
    UNDERLINE = '\033[4m'

fp = open(logfile, 'r')
while True:
        newline = fp.readline()
        # Once all lines are read this just returns ''
        # until the file changes and a new line appears

        if newline:

            r0 = re.compile(r'ERROR')
            r1 = re.compile(r'Role-based access disabled for this deployment|Authentication successful|Creating user session|Updating user session|Authenticating credentials')

            r2 = re.compile(r':MetricsReport|cenx-query')
      #leftovers from ares - replace these
            #r3 = re.compile(r'^\s+\[.*\]$')
            r3 = re.compile(r'at clojure|epiphany.components')
            r4 = re.compile(r'WARN')
            r5 = re.compile(r'^\s+at')
            r6 = re.compile(r'cenx.hecate.heart|com.cenx.service.logging.LogService.System|Added fetcher ')
            r7 = re.compile(r'kafka.consumer')
            r8 = re.compile(r':cause|:via|:type|:message|:at |:trace')  #various java clojure errors
            if r0.search(newline):
                print bcolors.FAIL + bcolors.BOLD + newline + bcolors.ENDC
            elif r1.search(newline):
                if debug : print bcolors.OKBLUE + newline + bcolors.ENDC
            elif r2.search(newline):
                if debug : print  bcolors.OKBLUE + newline + bcolors.ENDC
            elif r3.search(newline):
                if debug : print  bcolors.WARNING + newline + bcolors.ENDC
            elif r4.search(newline):
                 print  bcolors.WARNING + newline + bcolors.ENDC
            elif r5.search(newline):
                if debug : print bcolors.WARNING + newline + bcolors.ENDC
            elif r6.search(newline):
                if debug : print  bcolors.OKBLUE + newline + bcolors.ENDC
            elif r7.search(newline):
                if debug : print  bcolors.OKBLUE + newline + bcolors.ENDC
            elif r8.search(newline):
                if debug : print  bcolors.WARNING + newline + bcolors.ENDC
            else:
                #if debug : print "DEBUG good log entry found: "
                print bcolors.OKGREEN + newline + bcolors.ENDC
else:
else:
     time.sleep(0.5) 

 
